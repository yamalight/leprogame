exports.steam = {
  realm: process.env.LG_REALM || 'http://localhost:8080',
  apiKey: process.env.LG_STEAMKEY || 'YOUR_STEAM_KEY',
};

exports.session = {
  secret: process.env.LG_SECRET || 'secret',
  name: 'leprogamesess',
  resave: true,
  saveUninitialized: true,
};

exports.mongo = process.env.MONGO_URL || 'mongodb://localhost/leprogame';
