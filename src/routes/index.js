const setupPostRoutes = require('./posts');
const setupCommentRoutes = require('./comments');

module.exports = app => {
  setupPostRoutes(app);
  setupCommentRoutes(app);
};
