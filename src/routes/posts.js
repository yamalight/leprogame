const {Post} = require('../mongo');

module.exports = app => {
  app.post('/api/posts/new', async (req, res) => {
    try {
      const user = req.user._id;
      const postData = Object.assign({}, req.body, {user});
      const post = new Post(postData);
      await post.save();
      res.send(post.toObject());
    } catch (e) {
      res.status(500).send({message: 'Error creating post:', error: e});
    }
  });

  app.get('/api/posts', async (req, res) => {
    try {
      const posts = await Post.find({}).sort({date: 'desc'}).populate('user').exec();
      res.send(posts);
    } catch (e) {
      res.status(500).send({message: 'Error getting posts:', error: e});
    }
  });

  app.get('/api/posts/:id', async (req, res) => {
    try {
      const post = await Post.findOne({_id: req.params.id}).populate('user').exec();
      res.send(post);
    } catch (e) {
      res.status(500).send({message: 'Error getting post:', error: e});
    }
  });

  app.post('/api/posts/:id/upvote', async (req, res) => {
    try {
      const user = req.user._id;
      const post = await Post.findById(req.params.id).exec();

      // if already upvoted - just exit
      if (post.upvotes.includes(user)) {
        res.send(post);
        return;
      }

      // delete from downvotes
      const downIndex = post.downvotes.indexOf(user);
      if (downIndex !== -1) {
        post.downvotes.splice(downIndex, 1);
      }
      // push to upvotes
      post.upvotes.push(user);
      // save
      await post.save();
      // send back
      res.send(post);
    } catch (e) {
      res.status(500).send({message: 'Error upvoting post:', error: e});
    }
  });

  app.post('/api/posts/:id/downvote', async (req, res) => {
    try {
      const user = req.user._id;
      const post = await Post.findById(req.params.id).exec();

      // if already upvoted - just exit
      if (post.downvotes.includes(user)) {
        res.send(post);
        return;
      }

      // delete from upvotes
      const upIndex = post.upvotes.indexOf(user);
      if (upIndex !== -1) {
        post.upvotes.splice(upIndex, 1);
      }
      // push to downvotes
      post.downvotes.push(user);
      // save
      await post.save();
      // send back
      res.send(post);
    } catch (e) {
      res.status(500).send({message: 'Error upvoting post:', error: e});
    }
  });
};
