const {Comment} = require('../mongo');

module.exports = app => {
  app.post('/api/posts/:id/comments/new', async (req, res) => {
    const user = req.user._id;
    const post = req.params.id;
    const commentData = Object.assign({}, req.body, {user, post});
    try {
      const comment = new Comment(commentData);
      await comment.save();
      res.send(Object.assign(comment.toObject(), {user}));
    } catch (e) {
      res.status(500).send({message: 'Error creating comment:', error: e});
    }
  });

  app.get('/api/posts/:id/comments', async (req, res) => {
    try {
      const post = req.params.id;
      // get all comments for post
      const allComments = await Comment.find({post}).populate('user').exec();
      // console.log('got all comments:', allComments);
      const comments = [];
      const nodesMap = {}; // Keeps track of nodes using id as key, for fast lookup
      // loop over data
      allComments.map(c => c.toObject()).forEach(c => {
        // init replies array
        c.replies = []; // eslint-disable-line
        // add to lookup map
        nodesMap[c._id] = c;
        // check if there's parent
        if (!c.replyTo) {
          // if not - put to top level array
          comments.push(c);
        } else {
          // if yes - find parent
          const parentNode = nodesMap[c.replyTo];
          // and add current comment as reply
          parentNode.replies.push(c);
        }
      });
      res.send(comments);
    } catch (e) {
      res.status(500).send({message: 'Error getting comments:', error: e});
    }
  });

  app.post('/api/comments/:id/upvote', async (req, res) => {
    try {
      const user = req.user._id;
      const comment = await Comment.findById(req.params.id).exec();

      // if already upvoted - just exit
      if (comment.upvotes.includes(user)) {
        res.send(comment);
        return;
      }

      // delete from downvotes
      const downIndex = comment.downvotes.indexOf(user);
      if (downIndex !== -1) {
        comment.downvotes.splice(downIndex, 1);
      }
      // push to upvotes
      comment.upvotes.push(user);
      // save
      await comment.save();
      // send back
      res.send(comment);
    } catch (e) {
      res.status(500).send({message: 'Error upvoting comment:', error: e});
    }
  });

  app.post('/api/comments/:id/downvote', async (req, res) => {
    try {
      const user = req.user._id;
      const comment = await Comment.findById(req.params.id).exec();

      // if already upvoted - just exit
      if (comment.downvotes.includes(user)) {
        res.send(comment);
        return;
      }

      // delete from upvotes
      const upIndex = comment.upvotes.indexOf(user);
      if (upIndex !== -1) {
        comment.upvotes.splice(upIndex, 1);
      }
      // push to downvotes
      comment.downvotes.push(user);
      // save
      await comment.save();
      // send back
      res.send(comment);
    } catch (e) {
      res.status(500).send({message: 'Error upvoting comment:', error: e});
    }
  });
};
