// npm packages
const next = require('next');
const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const serializeError = require('serialize-error');

// our packages
const logger = require('./logger');
const {connected} = require('./mongo');
const {setupPassport} = require('./passport');
const setupRotues = require('./routes');

// create next.js instance
const dev = process.env.NODE_ENV !== 'production';
const app = next({dev});
const handle = app.getRequestHandler();

exports.start = async () => {
  // wait for db
  await connected;
  // prepare next.js
  await app.prepare();
  // create server
  const server = express();
  // configure middlewares
  server.use(cookieParser());
  server.use(bodyParser.urlencoded({extended: true}));
  server.use(bodyParser.json());
  // configure auth
  setupPassport(server);
  // setup server routes
  setupRotues(server);
  // handle all other requests with next.js
  server.get('/posts/:id', (req, res) => app.render(req, res, '/post', req.params));
  server.get('*', (req, res) => handle(req, res));
  // handle errors
  server.use((err, req, res, next) => app.render(req, res, '/error', {error: serializeError(err)})); // eslint-disable-line
  // start express server
  server.listen(8080, () => {
    logger.info('Server is started on :8080');
  });
};
