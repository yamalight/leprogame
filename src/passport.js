// npm packages
const url = require('url');
const cheerio = require('cheerio');
const request = require('request-promise-native');
const expressSession = require('express-session');
const passport = require('passport');
const SteamStrategy = require('passport-steam').Strategy;
const connectMongo = require('connect-mongo');

// our packages
const logger = require('./logger');
const {db, User} = require('./mongo');
const {steam, session: sessionCfg} = require('../config');

// init mongo persistent session storage
const MongoStore = connectMongo(expressSession);

// configure passport
passport.serializeUser((user, done) => done(null, user));
passport.deserializeUser((obj, done) => done(null, obj));
passport.use(
  new SteamStrategy(
    {
      returnURL: `${steam.realm}/auth/steam/return`,
      realm: steam.realm,
      apiKey: steam.apiKey,
    },
    async (identifier, profile, done) => {
      // check if user is already in db
      const existingUser = await User.findOne({identifier});
      if (existingUser) {
        logger.debug('Existing user found:', existingUser);
        done(null, existingUser.toObject());
        return;
      }

      // create new user
      const userData = {
        identifier,
        steamId: profile.id,
        username: profile.displayName,
        profileurl: profile._json.profileurl,
      };

      // check if user is in group
      const userPageUrl = userData.profileurl;
      const userGroupsUrl = url.resolve(userPageUrl, 'groups/');
      const body = await request(userGroupsUrl);
      const $ = cheerio.load(body);
      const playerGroups = $('div.groupBlock').map((index, el) => $('.linkTitle', el).text()).get();
      const isMember = playerGroups.includes('Leprogame');

      // authorize if member
      if (isMember) {
        const user = new User(userData);
        await user.save();
        logger.info('New user created:', user);
        done(null, user.toObject());
        return;
      }

      done(new Error('Not a member of Leprogame!'));
    }
  )
);

// main function
exports.setupPassport = app => {
  app.use(expressSession(Object.assign(sessionCfg, {store: new MongoStore({mongooseConnection: db})})));
  app.use(passport.initialize());
  app.use(passport.session());
  app.get('/auth/steam', passport.authenticate('steam'));
  app.get('/auth/steam/return', passport.authenticate('steam', {failureRedirect: '/login'}), (req, res) => {
    res.redirect('/');
  });
  app.get('/logout', (req, res) => {
    req.logout();
    res.redirect('/');
  });
  // setup auth check
  app.use((req, res, next) => {
    if (req.user !== undefined || req.originalUrl === '/login' || req.originalUrl.startsWith('/_next/')) {
      next();
      return;
    }

    res.redirect('/login');
  });
};
