// npm packages
const mongoose = require('mongoose');

// our packages
const createPost = require('./post');
const createUser = require('./user');
const createComment = require('./comment');
const config = require('../../config');

// Use native promises
mongoose.Promise = global.Promise;

// create connection
const db = mongoose.createConnection(config.mongo);

// connection promise
exports.connected = new Promise(resolve => db.on('connected', resolve));
// db instance
exports.db = db;
// models
exports.Post = createPost(db);
exports.User = createUser(db);
exports.Comment = createComment(db);
