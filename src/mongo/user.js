// npm packages
const mongoose = require('mongoose');

const UserSchema = new mongoose.Schema({
  steamid: String,
  username: String,
  identifier: String,
  steamId: String,
  profileurl: String,
});

module.exports = db => db.model('User', UserSchema);
