// npm packages
const mongoose = require('mongoose');

const PostSchema = new mongoose.Schema({
  date: {type: Date, default: Date.now},
  text: String,
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  upvotes: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  downvotes: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
});

module.exports = db => db.model('Post', PostSchema);
