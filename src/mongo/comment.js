// npm packages
const mongoose = require('mongoose');

const CommentSchema = new mongoose.Schema({
  date: {type: Date, default: Date.now},
  text: String,
  user: {type: mongoose.Schema.Types.ObjectId, ref: 'User'},
  post: {type: mongoose.Schema.Types.ObjectId, ref: 'Post'},
  replyTo: {type: mongoose.Schema.Types.ObjectId, ref: 'Comment'},
  upvotes: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
  downvotes: [{type: mongoose.Schema.Types.ObjectId, ref: 'User'}],
});

module.exports = db => db.model('Comment', CommentSchema);
