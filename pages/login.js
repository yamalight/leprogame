import React from 'react';

import Layout from './core/layout';

export default () =>
  <Layout>
    <section className="hero is-fullheight">
      <div className="hero-body">
        <div className="container has-text-centered">
          <h1 className="title">
            Leprogame
          </h1>
          <h2 className="subtitle">
            Standalone Edition
          </h2>
          <a href="/auth/steam">
            <img
              alt="Login with steam"
              src="https://steamcommunity-a.akamaihd.net/public/images/signinthroughsteam/sits_02.png"
            />
          </a>
        </div>
      </div>
    </section>
  </Layout>;
