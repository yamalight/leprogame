import React from 'react';

import Layout from './core/layout';
import Nav from './components/nav';
import Post from './components/post';
import Editor from './components/editor';
import Comments from './components/comments';

const fetchConfig = {
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'same-origin',
  method: 'POST',
};

export default class extends React.Component {
  static async getInitialProps({req, query}) {
    const postId = query.id;
    // add current user info
    if (req) {
      return {user: req.user, postId};
    }

    const userString = localStorage.getItem('user');
    if (userString) {
      const user = JSON.parse(userString);
      return {user, postId};
    }

    return {};
  }

  componentDidMount() {
    if (this.props.user) {
      localStorage.setItem('user', JSON.stringify(this.props.user));
    }
  }

  async submitComment(text) {
    const postId = this.props.postId;
    const body = JSON.stringify({text});
    const resp = await fetch(`/api/posts/${postId}/comments/new`, Object.assign(fetchConfig, {body}));
    if (resp.status !== 200) {
      throw new Error('Error creating new comment!');
    }
    const newComment = await resp.json();
    this.comments.addComment(newComment);
  }

  render() {
    const {user, postId} = this.props;

    return (
      <Layout>
        <Nav user={user} />
        <div className="container" style={{marginTop: 20}}>
          <Post postId={postId} user={user} hideComments />

          <br />
          Комментарии:
          <Comments
            ref={c => {
              this.comments = c;
            }}
            user={user}
            postId={postId}
          />

          <br />
          <Editor
            prompt={'Новый комментарий..'}
            collapsedPrompt={'Написать новый комментарий!'}
            submit={text => this.submitComment(text)}
          />
        </div>
      </Layout>
    );
  }
}
