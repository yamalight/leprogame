import React from 'react';

import Layout from './core/layout';
import Nav from './components/nav';
import Editor from './components/editor';
import Posts from './components/posts';

const fetchConfig = {
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'same-origin',
  method: 'POST',
};

export default class extends React.Component {
  static async getInitialProps({req}) {
    if (req) {
      return {user: req.user};
    }

    const userString = localStorage.getItem('user');
    if (userString) {
      const user = JSON.parse(userString);
      return {user};
    }

    return {user: undefined};
  }

  componentDidMount() {
    if (this.props.user) {
      localStorage.setItem('user', JSON.stringify(this.props.user));
    }
  }

  async submit(text) {
    const body = JSON.stringify({text});
    const resp = await fetch('/api/posts/new', Object.assign(fetchConfig, {body}));
    if (resp.status !== 200) {
      throw new Error('Error creating new post!');
    }
    const newPost = await resp.json();
    this.posts.addPost(newPost);
  }

  render() {
    return (
      <Layout>
        <Nav user={this.props.user} />
        <div className="container" style={{marginTop: 20}}>
          <Editor prompt={'Новый пост..'} collapsedPrompt={'Написать новый пост!'} submit={text => this.submit(text)} />
          <Posts
            user={this.props.user}
            ref={p => {
              this.posts = p;
            }}
          />
        </div>
      </Layout>
    );
  }
}
