import React from 'react';

import Layout from './core/layout';

export default class Error extends React.Component {
  static async getInitialProps({query}) {
    if (query) {
      return {error: query.error};
    }

    return {error: undefined};
  }

  render() {
    const {error} = this.props;

    return (
      <Layout>
        <div className="hero is-error is-fullheight">
          <div className="hero-body">
            <div className="container has-text-centered">
              <h1 className="title">
                {error.message}
              </h1>
              <h2 className="subtitle">
                {error.name}
              </h2>
              <pre style={{textAlign: 'left'}}>
                {error.stack}
              </pre>
            </div>
          </div>
        </div>
      </Layout>
    );
  }
}
