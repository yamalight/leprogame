import React from 'react';
import 'isomorphic-fetch';

import Comment from './comment';

export default class Comments extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      comments: [],
    };
  }

  async componentDidMount() {
    const {postId, urlBase} = this.props;
    const res = await fetch(`${urlBase || ''}/api/posts/${postId}/comments`, {
      credentials: 'same-origin',
    });
    const comments = await res.json();
    this.setState({comments});
  }

  addComment(comment) {
    const {comments} = this.state;
    const newComments = comments.concat([comment]);
    this.setState({comments: newComments});
  }

  render() {
    return (
      <div>
        {this.state.comments.map(c => <Comment key={c._id} comment={c} user={this.props.user} />)}
      </div>
    );
  }
}
