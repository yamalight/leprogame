import React from 'react';

import md from './markdown';

const fetchConfig = {
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'same-origin',
  method: 'POST',
};

export default class Post extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      post: props.post,
      postId: props.postId,
    };
  }

  async componentDidMount() {
    const {post, postId} = this.state;
    if (!post && postId && postId.length > 0) {
      const res = await fetch(`/api/posts/${postId}`, {
        credentials: 'same-origin',
      });
      const postData = await res.json();
      this.setState({post: postData});
    }
  }

  async upvote() {
    const {post} = this.state;
    const resp = await fetch(`/api/posts/${post._id}/upvote`, fetchConfig);
    if (resp.status !== 200) {
      throw new Error('Error upvoting post!');
    }
    const newPost = await resp.json();
    this.setState({post: newPost});
  }

  async downvote() {
    const {post} = this.state;
    const resp = await fetch(`/api/posts/${post._id}/downvote`, fetchConfig);
    if (resp.status !== 200) {
      throw new Error('Error downvoting post!');
    }
    const newPost = await resp.json();
    this.setState({post: newPost});
  }

  render() {
    const {post} = this.state;
    const {hideComments} = this.props;

    if (!post) {
      return (
        <div className="card">
          <div className="card-content content">
            Загружаем..
          </div>
        </div>
      );
    }

    const upvoted = post.upvotes.includes(this.props.user._id);
    const downvoted = post.downvotes.includes(this.props.user._id);

    return (
      <div className="card" style={{marginTop: 20}}>
        <div className="card-content content">
          <div dangerouslySetInnerHTML={{__html: md.render(post.text)}} />
          <br />
          <small>
            Написал
            {' '}
            {post.user.username}
            {' '}
            {new Date(post.date).toLocaleDateString()}
            {' '}
            в
            {' '}
            {new Date(post.date).toLocaleTimeString()}
            {' '}
            {hideComments ? '' : <a href={`/posts/${post._id}`}>Комментарии</a>}
          </small>
          {' '}
          <button
            className={`button is-small is-inverted ${!upvoted && 'is-info'}`}
            onClick={() => this.upvote()}
            disabled={upvoted}
          >
            <span className="icon is-small">
              <i className="fa fa-plus" />
            </span>
          </button>
          {' '}
          <button
            className={`button is-small is-inverted ${!downvoted && 'is-info'}`}
            onClick={() => this.downvote()}
            disabled={downvoted}
          >
            <span className="icon is-small">
              <i className="fa fa-minus" />
            </span>
          </button>
          {' '}
          <span className="tag is-light">
            {post.upvotes.map(() => 1).concat(post.downvotes.map(() => -1)).reduce((prev, v) => prev + v, 0)}
          </span>
        </div>
      </div>
    );
  }
}
