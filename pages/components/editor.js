import React from 'react';
import Textarea from 'react-textarea-autosize';
import 'isomorphic-fetch';

import md from './markdown';

export default class extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      expanded: false,
      text: '',
    };
  }

  toggle() {
    const {expanded, text} = this.state;

    // if not expanded - just expand
    if (!expanded) {
      this.setState({expanded: true}, () => {
        // focus new input
        this.textArea.focus();
      });
      return;
    }

    // if expanded - check for value
    if (text.length < 2) {
      this.setState({expanded: false});
    }
  }

  async submit() {
    const text = this.state.text;

    // do not submit empty text
    if (!text || text.length < 2) {
      return;
    }

    try {
      await this.props.submit(text);
      this.setState({expanded: false, text: ''});
    } catch (e) {
      console.error(e);
    }
  }

  render() {
    const {expanded, text} = this.state;

    if (!expanded) {
      return (
        <div className="field">
          <p className="control">
            <input
              className="input"
              type="text"
              placeholder={this.props.collapsedPrompt}
              onFocus={() => this.toggle()}
            />
          </p>
        </div>
      );
    }

    return (
      <div className="columns">
        <div className="column">
          <div className="field">
            <p className="control">
              <Textarea
                className="textarea"
                id="newpost"
                defaultValue={text}
                placeholder={this.props.prompt}
                inputRef={ta => (this.textArea = ta)}
                onChange={e => this.setState({text: e.target.value})}
                onBlur={() => this.toggle()}
              />
            </p>
          </div>
          <div className="field is-horizontal">
            <div className="field-body" style={{alignItems: 'flex-end', flexDirection: 'column'}}>
              <div className="field">
                <div className="control">
                  <button className="button is-primary" onClick={() => this.submit()}>
                    Yarrr!
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div className="column content" dangerouslySetInnerHTML={{__html: md.render(text)}} />
      </div>
    );
  }
}
