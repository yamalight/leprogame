import React from 'react';

export default ({user}) =>
  <nav className="nav has-shadow">
    <div className="container">
      <div className="nav-left">
        <a href="/" className="nav-item">Leprogame</a>
        <a href="/" className="nav-item is-tab">Главная</a>
      </div>
      <span className="nav-toggle">
        <span />
        <span />
        <span />
      </span>
      <div className="nav-right nav-menu">
        <a href="profile" className="nav-item is-tab">
          {user.username}
        </a>
        <a href="/logout" className="nav-item is-tab">Выйти</a>
      </div>
    </div>
  </nav>;
