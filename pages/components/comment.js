import React from 'react';

import md from './markdown';
import Editor from './editor';

const fetchConfig = {
  headers: {
    'Content-Type': 'application/json',
  },
  credentials: 'same-origin',
  method: 'POST',
};

export default class Comment extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      reply: false,
      comment: props.comment,
    };
  }

  toggleReply() {
    this.setState({reply: !this.state.reply});
  }

  async submitComment(text) {
    const postId = this.state.comment.post;
    const body = JSON.stringify({text, replyTo: this.state.comment._id});
    const resp = await fetch(`/api/posts/${postId}/comments/new`, Object.assign(fetchConfig, {body}));
    if (resp.status !== 200) {
      throw new Error('Error creating new comment!');
    }
    const reply = await resp.json();
    const {comment} = this.state;
    comment.replies.push(reply);
    this.setState({reply: false, comment});
  }

  async upvote() {
    const {comment} = this.state;
    const resp = await fetch(`/api/comments/${comment._id}/upvote`, fetchConfig);
    if (resp.status !== 200) {
      throw new Error('Error upvoting comment!');
    }
    const newComment = await resp.json();
    this.setState({comment: newComment});
  }

  async downvote() {
    const {comment} = this.state;
    const resp = await fetch(`/api/comments/${comment._id}/downvote`, fetchConfig);
    if (resp.status !== 200) {
      throw new Error('Error downvoting comment!');
    }
    const newComment = await resp.json();
    this.setState({comment: newComment});
  }

  render() {
    const {reply, comment} = this.state;
    const {user} = this.props;

    const upvoted = comment.upvotes.includes(user._id);
    const downvoted = comment.downvotes.includes(user._id);

    return (
      <div className="card" style={{boxShadow: 'none'}}>
        <div className="card-content content">
          <div dangerouslySetInnerHTML={{__html: md.render(comment.text)}} />
          {/* <pre>{JSON.stringify(this.props.comment, null, 2)}</pre> */}
          <br />
          <small>
            Написал
            {' '}
            {comment.user.username}
            {' '}
            {new Date(comment.date).toLocaleDateString()}
            {' '}
            в
            {' '}
            {new Date(comment.date).toLocaleTimeString()}
            {' '}
            <a href="#reply" onClick={() => this.toggleReply()}>Ответить</a>
          </small>
          {' '}
          <button
            className={`button is-small is-inverted ${!upvoted && 'is-info'}`}
            onClick={() => this.upvote()}
            disabled={upvoted}
          >
            <span className="icon is-small">
              <i className="fa fa-plus" />
            </span>
          </button>
          {' '}
          <button
            className={`button is-small is-inverted ${!downvoted && 'is-info'}`}
            onClick={() => this.downvote()}
            disabled={downvoted}
          >
            <span className="icon is-small">
              <i className="fa fa-minus" />
            </span>
          </button>
          {' '}
          <span className="tag is-light">
            {comment.upvotes.map(() => 1).concat(comment.downvotes.map(() => -1)).reduce((prev, v) => prev + v, 0)}
          </span>

          {reply &&
            <Editor
              prompt={'Новый комментарий..'}
              collapsedPrompt={'Написать новый комментарий!'}
              submit={text => this.submitComment(text)}
            />}

          {comment.replies &&
            comment.replies.length > 0 &&
            <div className="columns" style={{marginTop: 10}}>
              <div className="column is-1" />
              <div className="column">{comment.replies.map(c => <Comment key={c._id} user={user} comment={c} />)}</div>
            </div>}
        </div>
      </div>
    );
  }
}
