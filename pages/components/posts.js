import React from 'react';
import 'isomorphic-fetch';

import Post from './post';

export default class Posts extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      posts: [],
    };
  }

  async componentDidMount() {
    const {urlBase} = this.props;
    const res = await fetch(`${urlBase || ''}/api/posts`, {
      credentials: 'same-origin',
    });
    const posts = await res.json();
    this.setState({posts});
  }

  addPost(post) {
    const {posts} = this.state;
    const newPosts = posts.concat([post]);
    this.setState({posts: newPosts});
  }

  render() {
    return (
      <div>
        {this.state.posts.map(p => <Post key={p._id} post={p} user={this.props.user} />)}
      </div>
    );
  }
}
