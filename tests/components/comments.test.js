/* eslint-env jest */
// npm packages
import React from 'react';
import renderer from 'react-test-renderer';
import nock from 'nock';

// our packages
import Comments from '../../pages/components/comments';
import {testComment, testUser} from '../__fixtures__';

beforeAll(() => {
  nock('http://localhost').get('/api/posts/1/comments').reply(200, [testComment]);
});

test('# Comments', done => {
  const wrapper = renderer.create(<Comments user={testUser} postId={'1'} urlBase={'http://localhost'} />);
  setTimeout(() => {
    expect(wrapper.toJSON()).toMatchSnapshot();
    done();
  }, 100);
});
