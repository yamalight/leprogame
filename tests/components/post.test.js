/* eslint-env jest */
// npm packages
import React from 'react';
import renderer from 'react-test-renderer';

// our packages
import Post from '../../pages/components/post';
import {testPost, testUser} from '../__fixtures__';

test('# Post', () => {
  const wrapper = renderer.create(<Post user={testUser} post={testPost} />).toJSON();
  expect(wrapper).toMatchSnapshot();
});
