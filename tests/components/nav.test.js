/* eslint-env jest */
// npm packages
import React from 'react';
import renderer from 'react-test-renderer';

// our packages
import Nav from '../../pages/components/nav';
import {testUser} from '../__fixtures__';

test('# Nav', () => {
  const wrapper = renderer.create(<Nav user={testUser} />).toJSON();
  expect(wrapper).toMatchSnapshot();
});
