/* eslint-env jest */
// npm packages
import React from 'react';
import renderer from 'react-test-renderer';

// our packages
import Comment from '../../pages/components/comment';
import {testComment, testUser} from '../__fixtures__';

test('# Comment', () => {
  const wrapper = renderer.create(<Comment user={testUser} comment={testComment} />).toJSON();
  expect(wrapper).toMatchSnapshot();
});
