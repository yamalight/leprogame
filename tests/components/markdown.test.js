/* eslint-env jest */
// our packages
import md from '../../pages/components/markdown';

test('# Markdown', () => {
  const wrapper = md.render(`# Test markdown

- a
- b
- c`);
  expect(wrapper).toMatchSnapshot();
});
