/* eslint-env jest */
// npm packages
import React from 'react';
import renderer from 'react-test-renderer';
import nock from 'nock';

// our packages
import Posts from '../../pages/components/posts';
import {testPost, testUser} from '../__fixtures__';

beforeAll(() => {
  nock('http://localhost').get('/api/posts').reply(200, [testPost]);
});

test('# Posts', done => {
  const wrapper = renderer.create(<Posts user={testUser} urlBase="http://localhost" />);
  setTimeout(() => {
    expect(wrapper.toJSON()).toMatchSnapshot();
    done();
  }, 100);
});
