/* eslint-env jest */
// npm packages
import React from 'react';
import renderer from 'react-test-renderer';

// our packages
import Editor from '../../pages/components/editor';
import {testUser} from '../__fixtures__';

test('# Editor', () => {
  const wrapper = renderer.create(<Editor user={testUser} prompt="Test" />);
  expect(wrapper.toJSON()).toMatchSnapshot();
});
