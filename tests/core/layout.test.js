/* eslint-env jest */
// npm packages
import React from 'react';
import renderer from 'react-test-renderer';

// our packages
import Layout from '../../pages/core/layout';

test('# Layout', () => {
  const wrapper = renderer.create(<Layout><div /></Layout>);
  expect(wrapper.toJSON()).toMatchSnapshot();
});
