/* eslint-env jest */
// npm packages
import React from 'react';
import renderer from 'react-test-renderer';

// our packages
import Login from '../../pages/login';

test('# Layout', () => {
  const wrapper = renderer.create(<Login />);
  expect(wrapper.toJSON()).toMatchSnapshot();
});
