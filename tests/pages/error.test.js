/* eslint-env jest */
// npm packages
import React from 'react';
import renderer from 'react-test-renderer';

// our packages
import Error from '../../pages/error';

test('# Layout', () => {
  const testError = {
    message: 'Test error message',
    name: 'Test error',
    stack: 'Test error stack',
  };
  const wrapper = renderer.create(<Error error={testError} />);
  expect(wrapper.toJSON()).toMatchSnapshot();
});
