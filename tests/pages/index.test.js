/* eslint-env jest */
// npm packages
import React from 'react';
import ReactShallowRenderer from 'react-test-renderer/shallow';

// our packages
import '../__mocks__';
import Index from '../../pages/index';
import {testUser} from '../__fixtures__';

test('# Layout', () => {
  const renderer = new ReactShallowRenderer();
  const result = renderer.render(<Index user={testUser} postId={'1'} />);
  expect(result).toMatchSnapshot();
});
