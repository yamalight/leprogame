/* eslint-env jest */
// npm modules
const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');

// our modules
const posts = require('../../src/routes/posts');
const {db, User} = require('../../src/mongo');

// storage vars
let user = {};
let newPost = {};

beforeAll(async done => {
  // create test user
  const mongoUser = new User({
    steamid: 'test_id',
    username: 'test_user',
    identifier: 'test_identifier',
    steamId: 'test_steam_id',
    profileurl: 'http://test.url',
  });
  await mongoUser.save();
  user = mongoUser.toObject();
  done();
});

// init new express app
const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// append test user to request
app.use(async (req, res, next) => {
  req.user = user;
  next();
});
// apply post routes
posts(app);

test('# Should create new post', done => {
  const testText = 'Test post';

  request(app).post('/api/posts/new').send({text: testText}).expect(200).end((err, res) => {
    if (err) {
      throw err;
    }

    newPost = res.body;
    expect(newPost.user).toEqual(user._id.toString());
    expect(newPost.text).toEqual(testText);
    done();
  });
});

test('# Should get posts', done => {
  request(app).get('/api/posts').expect(200).end((err, res) => {
    if (err) {
      throw err;
    }

    const allPosts = res.body.filter(p => p.text !== 'Test comment post');
    expect(allPosts.length).toEqual(1);
    expect(allPosts[0].user._id).toEqual(newPost.user);
    expect(allPosts[0].user.username).toEqual(user.username);
    expect(allPosts[0].text).toEqual(newPost.text);
    expect(allPosts[0].date).toEqual(newPost.date);
    done();
  });
});

test('# Should get created post', done => {
  request(app).get(`/api/posts/${newPost._id}`).expect(200).end((err, res) => {
    if (err) {
      throw err;
    }

    const fetchedPost = res.body;
    expect(fetchedPost.user._id).toEqual(newPost.user);
    expect(fetchedPost.user.username).toEqual(user.username);
    expect(fetchedPost.text).toEqual(newPost.text);
    expect(fetchedPost.date).toEqual(newPost.date);
    done();
  });
});

test('# Should upvote new post', done => {
  request(app).post(`/api/posts/${newPost._id}/upvote`).send({}).expect(200).end((err, res) => {
    if (err) {
      throw err;
    }

    const upvotedPost = res.body;
    expect(upvotedPost.upvotes.length).toEqual(1);
    expect(upvotedPost.upvotes[0]).toEqual(user._id.toString());
    done();
  });
});

test('# Should downvote new post', done => {
  request(app).post(`/api/posts/${newPost._id}/downvote`).send({}).expect(200).end((err, res) => {
    if (err) {
      throw err;
    }

    const downvotedPost = res.body;
    expect(downvotedPost.downvotes.length).toEqual(1);
    expect(downvotedPost.downvotes[0]).toEqual(user._id.toString());
    expect(downvotedPost.upvotes.length).toEqual(0);
    done();
  });
});

afterAll(() => {
  db.close();
});
