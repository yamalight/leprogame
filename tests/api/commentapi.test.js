/* eslint-env jest */
// npm modules
const request = require('supertest');
const express = require('express');
const bodyParser = require('body-parser');

// our modules
const comments = require('../../src/routes/comments');
const {db, User, Post} = require('../../src/mongo');

// storage vars
let user = {};
let post = {};
let newComment = {};

beforeAll(async done => {
  // create test user
  const mongoUser = new User({
    steamid: 'test_id',
    username: 'test_user',
    identifier: 'test_identifier',
    steamId: 'test_steam_id',
    profileurl: 'http://test.url',
  });
  await mongoUser.save();
  user = mongoUser.toObject();
  // create test post
  const mongoPost = new Post({
    text: 'Test comment post',
    user: user._id,
  });
  await mongoPost.save();
  post = mongoPost.toObject();
  done();
});

// init new express app
const app = express();
app.use(bodyParser.urlencoded({extended: true}));
app.use(bodyParser.json());
// append test user to request
app.use((req, res, next) => {
  req.user = user;
  next();
});
// apply comment routes
comments(app);

test('# Should create new comment', done => {
  const testText = 'Test comment';

  request(app).post(`/api/posts/${post._id}/comments/new`).send({text: testText}).expect(200).end((err, res) => {
    if (err) {
      throw err;
    }

    newComment = res.body;
    expect(newComment.user).toEqual(user._id.toString());
    expect(newComment.post).toEqual(post._id.toString());
    expect(newComment.text).toEqual(testText);
    done();
  });
});

test('# Should get post comments', done => {
  request(app).get(`/api/posts/${post._id}/comments`).expect(200).end((err, res) => {
    if (err) {
      throw err;
    }

    const allComments = res.body;
    expect(allComments.length).toEqual(1);
    expect(allComments[0].user._id).toEqual(newComment.user);
    expect(allComments[0].user.username).toEqual(user.username);
    expect(allComments[0].text).toEqual(newComment.text);
    expect(allComments[0].date).toEqual(newComment.date);
    done();
  });
});

test('# Should upvote new comment', done => {
  request(app).post(`/api/comments/${newComment._id}/upvote`).send({}).expect(200).end((err, res) => {
    if (err) {
      throw err;
    }

    const upvotedComment = res.body;
    expect(upvotedComment.upvotes.length).toEqual(1);
    expect(upvotedComment.upvotes[0]).toEqual(user._id.toString());
    done();
  });
});

test('# Should downvote new comment', done => {
  request(app).post(`/api/comments/${newComment._id}/downvote`).send({}).expect(200).end((err, res) => {
    if (err) {
      throw err;
    }

    const downvotedComment = res.body;
    expect(downvotedComment.downvotes.length).toEqual(1);
    expect(downvotedComment.downvotes[0]).toEqual(user._id.toString());
    expect(downvotedComment.upvotes.length).toEqual(0);
    done();
  });
});

afterAll(() => {
  db.close();
});
