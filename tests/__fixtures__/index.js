export const testComment = {
  _id: 'test_comment',
  post: 'test_post_id',
  text: 'Test comment',
  date: new Date(2017, 1, 1, 0, 0, 0, 0),
  user: {username: 'Test user'},
  replies: [],
  upvotes: [],
  downvotes: [],
};

export const testUser = {
  _id: 'test_user_id',
  username: 'test',
};

export const testPost = {
  _id: 'test_post',
  text: 'Tets post',
  date: new Date(2017, 1, 1, 0, 0, 0, 0),
  user: {username: 'Test user'},
  upvotes: [],
  downvotes: [],
};
