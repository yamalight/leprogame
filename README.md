# Leprogame

[![build status](https://gitlab.com/yamalight/leprogame/badges/master/build.svg)](https://gitlab.com/yamalight/leprogame/commits/master)
[![coverage report](https://gitlab.com/yamalight/leprogame/badges/master/coverage.svg)](https://gitlab.com/yamalight/leprogame/commits/master)
[![Docker Pulls](https://img.shields.io/docker/pulls/leprogame/leprogame.svg?maxAge=2592000)](https://hub.docker.com/r/leprogame/leprogame/)
[![Docker image size](https://images.microbadger.com/badges/image/leprogame/leprogame.svg)](https://microbadger.com/images/leprogame/leprogame)
[![license](https://img.shields.io/github/license/mashape/apistatus.svg?maxAge=2592000)](https://opensource.org/licenses/MIT)

Уютный бложик про игры. Отдельни, опенсорсни, мжвячни!

## Требуемые штуки

Если вы хотите просто запустить эту пепяку то нужен только [Docker](https://docs.docker.com/engine/installation/) и [docker-compose](https://docs.docker.com/compose/install/) (обычно ставится вместе с первым).

Если вы хотите допиливать эту пепяку, то в довесок к докеру нужно поставить [Node.js](https://nodejs.org/) и [yarn](https://yarnpkg.com/).

## Запускаем с докером

1. Клонируем этот репозиторий
2. Редактируем `docker-compose.yml` и прописываем в `environment` свои данные - главное вписать `LG_REALM` (адрес вашего сайта для аутентификации в стим, чаще всего можно оставить `localhost`) и `LG_STEAMKEY` (ключ стим API, получается [тут](http://steamcommunity.com/dev/apikey))
3. Запускаем `docker-compose up`
4. Открываем [http://localhost](http://localhost) в браузере

## Запускаем с нодой (для разработки)

1. Клонируем этот репозиторий
2. Редактируем `config.js` и прописываем свои данные
3. Устанавливаем зависимости с помощь `yarn`
4. Запускаем mongodb (либо свою руками, либо через докер с помощью `yarn db:run`)
5. Запускаем сам сервис с помощью `yarn dev`
6. Открываем [http://localhost:8080](http://localhost:8080) в браузере

## Как это написано?

Весь проекта написан на [Node.js](https://nodejs.org/) и использует [MongoDB](https://www.mongodb.com/) как базу данных.  
API сделано на базе [Express.js](http://expressjs.com/), а фронт использует [Next.js](https://github.com/zeit/next.js/) (фреймворк на базе [React.js](https://facebook.github.io/react/) с поддержкой server-side rendering, автоматическим роутингом и прочими полезными штуками).  
В данный момент авторизация просто проверяет состоит ли юзер в группе [Leprogame](http://steamcommunity.com/groups/Leprogame) в стиме.

## Лицензия 

[MIT](https://opensource.org/licenses/MIT)
